import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import '../model/user_info_model.dart';

class UserInfoScreen extends StatefulWidget {
   UserInfoScreen({Key? key}) : super(key: key);

  @override
  State<UserInfoScreen> createState() => _UserInfoScreenState();
}

class _UserInfoScreenState extends State<UserInfoScreen> {
  final TextEditingController _fullnameController = TextEditingController();

  final TextEditingController _ageController = TextEditingController();

  final TextEditingController _cityController = TextEditingController();
  List<UserInfo> _users =[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('hive practice'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(
              flex: 3,
                child:
            Column(
              children: [
                TextField(
                  controller: _fullnameController,
                  style: Theme.of(context).textTheme.bodyText2,
                  decoration: InputDecoration(
                      hintText: 'fullname',
                      hintStyle: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey, height: 3)
                  ),

                ),
                TextField(
                  controller: _ageController,
                  style: Theme.of(context).textTheme.bodyText2,
                  decoration: InputDecoration(
                      hintText: 'age',
                      hintStyle: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey, height: 3)
                  ),

                ),
                TextField(
                  controller: _cityController,
                  style: Theme.of(context).textTheme.bodyText2,
                  decoration: InputDecoration(
                      hintText: 'city',
                      hintStyle: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.grey, height: 3)
                  ),

                ),
                SizedBox(
                  height: 30,
                ),
                TextButton(onPressed: () {_saveData();}, child: Text('Save Data')),
                TextButton(onPressed: () {_loadData();}, child: Text('Load Data')),
                TextButton(onPressed: () {_clearData();}, child: Text('Clear Data'))
              ],
            )),
            Flexible(
              flex: 2,
              child: ListView(
              children: [
                for(var item in _users)
                Container(
                  child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("${item.fullName}  (${item.age})  from ${item.city}" , style: TextStyle(fontSize: 20),),
                      )),
                )
              ],
            ),),



          ],
        ),
      )
    );
  }

  void _saveData() async{
    String fullName = _fullnameController.text;
    int age = int.parse(_ageController.text);
    String city = _cityController.text;
    var user = UserInfo(fullName: fullName, age: age, city: city);
    var box = await Hive.openBox('userInfo');
    box.add(user);

    await box.close();

    _loadData();

    print('save Data');
  }
  void _loadData() async{
    var box = await Hive.openBox<UserInfo>('userInfo');
    _users=[];
    box.values.forEach((element) {
      _users.add(element);
    });


    box.close();
    setState(() {
    });
    print('load Data');
  }

  void _saveDataBymodel() async{
    String fullName = _fullnameController.text;
    int age = int.parse(_ageController.text);
    String city = _cityController.text;
    var user = UserInfo(fullName: fullName, age: age, city: city);
    var box = await Hive.openBox('userInfo');
    box.put('user', user);
    box.close();

    print('save Data');
  }
  void _loadDataByModel() async{
    var box = await Hive.openBox('userInfo');
    UserInfo user = box.get('user');
    _fullnameController.text = user.fullName;
    _ageController.text = user.age.toString();
    _cityController.text = user.city.toString();
    box.close();
    setState(() {
    });
    print('load Data');
  }

  void _clearData() async{
    var box = await Hive.openBox('userInfo');
    box.clear();
    print('clear Data');
  }

  void _saveDataBySimpleWay() async{
    String fullName = _fullnameController.text;
    int age = int.parse(_ageController.text);
    String city = _cityController.text;
    // var user = UserInfo(fullName: fullName, age: age, city: city)
    var box = await Hive.openBox('userInfo');
    box.put('fullname', fullName);
    box.put('age', age);
    box.put('city', city);

    box.close();

    print('save Data');
  }

  void _loadDataBySimpleWay() async{
    var box = await Hive.openBox('userInfo');
    _fullnameController.text = box.get('fullname');
    _ageController.text = box.get('age').toString();
    _cityController.text = box.get('city');
    box.close();
    setState(() {
    });
    print('load Data');
  }


}
